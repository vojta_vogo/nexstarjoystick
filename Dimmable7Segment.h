#ifndef __DIMABLE_7SEGMENT_H__
#define __DIMABLE_7SEGMENT_H__
#include <Arduino.h>

class Dimable7Segment
{
public:
    Dimable7Segment(uint8_t aPin, uint8_t bPin, uint8_t cPin, uint8_t dPin,
                    uint8_t ePin, uint8_t fPin, uint8_t gPin, uint8_t dpPin);
    void begin();
    // Sets active segments
    void setSegments(uint8_t segments);
    // Sets display intensity 0~8
    void setIntensity(uint8_t intensity);
};

#endif