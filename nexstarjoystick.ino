#include <Arduino.h>
#include "AuxNexStar.h"
#include "Button.h"
#include "Dimmable7Segment.h"

// Wiring
#define AUX_BUSY_PIN 10

#define JOYSTICK_PIN_UP A2
#define JOYSTICK_PIN_DOWN A3
#define JOYSTICK_PIN_RIGHT A1
#define JOYSTICK_PIN_LEFT A0
#define JOYSTICK_PIN_STOP 12
#define JOYSTICK_PIN_PLUS A5
#define JOYSTICK_PIN_MINUS A4

#define SEGMENT_PIN_A 2
#define SEGMENT_PIN_B 3
#define SEGMENT_PIN_C 4
#define SEGMENT_PIN_D 5
#define SEGMENT_PIN_E 6
#define SEGMENT_PIN_F 7
#define SEGMENT_PIN_G 8
#define SEGMENT_PIN_DP 9

// AUX messages
#define MSG_MC_SET_POS_GUIDERATE 0x06
#define MSG_MC_SET_NEG_GUIDERATE 0x07
#define MSG_MC_MOVE_POS 0x24
#define MSG_MC_MOVE_NEG 0x25
#define MSG_WIFI_STATUS 0x10

// Interface IDs
#define DEV_AZM 0x10
#define DEV_ALT 0x11
#define DEV_WIFI 0xb5
#define DEV_JOYSTICK 0xef

Dimable7Segment segment(SEGMENT_PIN_A, SEGMENT_PIN_B, SEGMENT_PIN_C, SEGMENT_PIN_D,
                        SEGMENT_PIN_E, SEGMENT_PIN_F, SEGMENT_PIN_G, SEGMENT_PIN_DP);

Aux aux(AUX_BUSY_PIN);

struct
{
    Button button;
    uint8_t device;
    uint8_t message;
} direction[4] = {
    {Button(JOYSTICK_PIN_UP), DEV_ALT, MSG_MC_MOVE_POS},
    {Button(JOYSTICK_PIN_DOWN), DEV_ALT, MSG_MC_MOVE_NEG},
    {Button(JOYSTICK_PIN_RIGHT), DEV_AZM, MSG_MC_MOVE_POS},
    {Button(JOYSTICK_PIN_LEFT), DEV_AZM, MSG_MC_MOVE_NEG},
};

Button stop(JOYSTICK_PIN_STOP);
Button plus(JOYSTICK_PIN_PLUS);
Button minus(JOYSTICK_PIN_MINUS);

// Display flags
bool showSlewRate = true;
bool showDecimalPoint = false;
// Default slew rate
uint8_t slewRate = 5;
// Default intensity
uint8_t intensity = 4;
// Show slew rate with 5 seconds timeout
bool slewRateDisplayed = false;
unsigned long slewRateMillis = 0;
// Show decimal dot with 250 miliseconds timeout
bool decimalPointDisplayed = false;
unsigned long decimalPointMillis = 0;

// 7-segment display
//     A
//    ---
// F |   | B
//   | G |
//    ---
// E |   | C
//   |   |
//    ---  .
//     D   DP

const uint8_t segmentSymbol[] = {
    0b00111111,
    0b00000110,
    0b01011011,
    0b01001111,
    0b01100110,
    0b01101101,
    0b01111101,
    0b00000111,
    0b01111111,
    0b01101111,
};

void refreshDisplay()
{
    unsigned long currentMillis = millis();

    if (showSlewRate)
    {
        slewRateDisplayed = true;
        slewRateMillis = currentMillis;
    }
    else if (slewRateDisplayed && (currentMillis - slewRateMillis) > 5000)
    {
        slewRateDisplayed = false;
    }

    if (showDecimalPoint)
    {
        decimalPointDisplayed = true;
        decimalPointMillis = currentMillis;
    }
    else if (decimalPointDisplayed && (currentMillis - decimalPointMillis) > 250)
    {
        decimalPointDisplayed = false;
    }

    uint8_t segments = 0b00000000;

    if (decimalPointDisplayed)
    {
        segments |= 0b10000000;
    }

    if (slewRateDisplayed)
    {
        segments |= segmentSymbol[slewRate];
    }

    segment.setSegments(segments);

    showSlewRate = showDecimalPoint = false;
}

// Store last used guide rate
uint8_t altGuideRateDataLength = 0;
uint8_t altGuideRateData[6] = {DEV_JOYSTICK, DEV_ALT, 0, 0, 0, 0};
uint8_t azmGuideRateDataLength = 0;
uint8_t azmGuideRateData[6] = {DEV_JOYSTICK, DEV_AZM, 0, 0, 0, 0};

void storeGuideRate()
{
    // Check message and store guide rate for each motor controller
    if ((aux.getDataLength() > 2) && (aux.getDataLength() < 7) &&
        ((aux.getData(2) == MSG_MC_SET_POS_GUIDERATE) || (aux.getData(2) == MSG_MC_SET_NEG_GUIDERATE)))
    {
        switch (aux.getData(1))
        {
        case DEV_ALT:
            altGuideRateDataLength = aux.getDataLength();

            for (uint8_t i = 2; i < altGuideRateDataLength; i++)
            {
                altGuideRateData[i] = aux.getData(i);
            }
            break;

        case DEV_AZM:
            azmGuideRateDataLength = aux.getDataLength();

            for (uint8_t i = 2; i < azmGuideRateDataLength; i++)
            {
                azmGuideRateData[i] = aux.getData(i);
            }
            break;

        default:
            break;
        }
    }
}

void sendSlewMessage(uint8_t dstDev, uint8_t msgId, uint8_t value)
{
    uint8_t data[4] = {DEV_JOYSTICK, dstDev, msgId, value};

    if (aux.sendData(data, 4))
    {
        showDecimalPoint = true;
    }

    // Restore guide rate
    if (value == 0)
    {
        switch (dstDev)
        {
        case DEV_ALT:
            if (altGuideRateDataLength)
            {
                aux.sendData(altGuideRateData, altGuideRateDataLength);
            }
            break;

        case DEV_AZM:
            if (azmGuideRateDataLength)
            {
                aux.sendData(azmGuideRateData, azmGuideRateDataLength);
            }
            break;
        }
    }
}

void toggleWiFi()
{
    uint8_t data[4] = {DEV_JOYSTICK, DEV_WIFI, MSG_WIFI_STATUS, 0};
    // Get actual state
    if (aux.sendData(data, 3))
    {
        // Wait for response
        for (unsigned long millis0 = millis(); (millis() - millis0) < 5000; delay(100))
        {
            while (aux.decode())
            {
                storeGuideRate();
                // Check response message
                if ((aux.getDataLength() > 4) &&
                    (aux.getData(0) == DEV_WIFI) && (aux.getData(1) == DEV_JOYSTICK) && (aux.getData(2) == MSG_WIFI_STATUS))
                {
                    // Toggle state
                    data[3] = aux.getData(4) ? 0 : 1;

                    if (aux.sendData(data, 4))
                    {
                        showDecimalPoint = true;
                    }

                    return;
                }
            }
        }
    }
}

void setup()
{
    // Init AUX
    aux.begin();

    // Init 7-segment
    segment.begin();
    segment.setIntensity(intensity);

    // Init direcion buttons
    for (uint8_t i = 0; i < 4; i++)
    {
        direction[i].button.attach();
    }

    // Init other buttons
    stop.attach();
    plus.attach();
    minus.attach();
}

void loop()
{
    refreshDisplay();

    // Process AUX messages
    while (aux.decode())
    {
        storeGuideRate();
    }

    // Send new slew rate if changed and slew in progress
    bool slewRateChange = false;

    // Handle plus button
    plus.refresh();
    // Hold plus for change display intensity, short click for change slew speed
    if (plus.isClickRepeat())
    {
        showSlewRate = true;

        if (intensity < 7)
        {
            segment.setIntensity(++intensity);
        }
    }
    else if (plus.isShortClick())
    {
        // Change slew rate only when slew rate is displayed
        if (slewRateDisplayed && (slewRate < 9))
        {
            slewRate++;
            slewRateChange = true;
        }

        showSlewRate = true;
    }

    // Handle minus button
    minus.refresh();
    // Hold minus for change display intensity, short click for change slew speed
    if (minus.isClickRepeat())
    {
        showSlewRate = true;

        if (intensity > 1)
        {
            segment.setIntensity(--intensity);
        }
    }
    else if (minus.isShortClick())
    {
        // Change slew rate only when slew rate is displayed
        if (slewRateDisplayed && (slewRate > 1))
        {
            slewRate--;
            slewRateChange = true;
        }

        showSlewRate = true;
    }

    // Handle direction buttons
    for (uint8_t i = 0; i < 4; i++)
    {
        direction[i].button.refresh();

        if (direction[i].button.isPushed())
        {
            showSlewRate = true;
        }

        if (direction[i].button.isChange() || (slewRateChange && direction[i].button.isPushed()))
        {
            sendSlewMessage(direction[i].device, direction[i].message, direction[i].button.isPushed() ? slewRate : 0);
        }
    }

    // Handle stop button
    stop.refresh();

    if (stop.isPushed())
    {
        showSlewRate = true;
    }

    // Short click stop for emergency stop
    if (stop.isShortClick())
    {
        sendSlewMessage(DEV_ALT, MSG_MC_MOVE_POS, 0);
        sendSlewMessage(DEV_AZM, MSG_MC_MOVE_POS, 0);
    }
    // Long click stop for toggle WiFi
    if (stop.isLongClick())
    {
        toggleWiFi();
    }
}
