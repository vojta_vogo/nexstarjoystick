#include "Dimmable7Segment.h"

volatile uint8_t _pin[8] = {};
volatile uint8_t _mask = 0b00000000;
volatile uint8_t _duty = 0;
volatile uint8_t _cycle = 0;
volatile uint8_t _high = false;

void setTimer()
{
    // http://www.8bit-era.cz/arduino-timer-interrupts-calculator.html
    // TIMER 1 for interrupt frequency 20000 Hz:
    cli();      // stop interrupts
    TCCR1A = 0; // set entire TCCR1A register to 0
    TCCR1B = 0; // same for TCCR1B
    TCNT1 = 0;  // initialize counter value to 0
    // set compare match register for 20000 Hz increments
    OCR1A = 799; // = 16000000 / (1 * 20000) - 1 (must be <65536)
    // turn on CTC mode
    TCCR1B |= (1 << WGM12);
    // Set CS12, CS11 and CS10 bits for 1 prescaler
    TCCR1B |= (0 << CS12) | (0 << CS11) | (1 << CS10);
    // enable timer compare interrupt
    TIMSK1 |= (1 << OCIE1A);
    sei(); // allow interrupts
}

ISR(TIMER1_COMPA_vect)
{
    if (!_high && !_cycle && _duty)
    {
        _high = true;

        for (uint8_t i = 0; i < 8; i++)
        {
            digitalWrite(_pin[i], bitRead(_mask, i) ? HIGH : LOW);
        }
    }
    else if (_high && (_cycle > _duty))
    {
        _high = false;

        for (uint8_t i = 0; i < 8; i++)
        {
            digitalWrite(_pin[i], LOW);
        }
    }

    _cycle++;
}

Dimable7Segment::Dimable7Segment(uint8_t aPin, uint8_t bPin, uint8_t cPin, uint8_t dPin,
                                 uint8_t ePin, uint8_t fPin, uint8_t gPin, uint8_t dpPin)
{
    _pin[0] = aPin;
    _pin[1] = bPin;
    _pin[2] = cPin;
    _pin[3] = dPin;
    _pin[4] = ePin;
    _pin[5] = fPin;
    _pin[6] = gPin;
    _pin[7] = dpPin;
}

void Dimable7Segment::begin()
{
    // Init pins
    for (uint8_t i = 0; i < 8; i++)
    {
        pinMode(_pin[i], OUTPUT);
    }
    // Init timer
    setTimer();
}

void Dimable7Segment::setSegments(uint8_t segments)
{
    if (_mask != segments)
    {
        _mask = segments;
    }
}

void Dimable7Segment::setIntensity(uint8_t intensity)
{
    uint8_t duty = min(pow(2, intensity) - 1, UINT8_MAX);

    if (_duty != duty)
    {
        _duty = duty;
    }
}