#include "Button.h"

#define BUTTON_SHORT_CLICK_MILLIS 50
#define BUTTON_REPEAT_MILLIS 500
#define BUTTON_LONG_CLICK_MILLIS 1000

Button::Button(uint8_t pin)
    : buttonPin(pin)
{
}

void Button::attach()
{
    pinMode(buttonPin, INPUT_PULLUP);
}

void Button::refresh()
{
    prevPushed = pushed;
    pushed = (buttonPin < A0) ? !digitalRead(buttonPin) : (analogRead(buttonPin) < 512);

    if (pushed != prevPushed)
    {
        if (pushed)
        {
            pushMillis = millis();
            longClickPassed = false;
        }
    }
}

bool Button::isPushed()
{
    return pushed;
}

bool Button::isChange()
{
    return pushed != prevPushed;
}

bool Button::isShortClick()
{
    unsigned long currentMillis = millis();

    if (prevPushed && !pushed &&
        ((currentMillis - pushMillis) >= BUTTON_SHORT_CLICK_MILLIS) &&
        ((currentMillis - pushMillis) < BUTTON_REPEAT_MILLIS))
    {
        return true;
    }

    return false;
}

bool Button::isLongClick()
{
    if (pushed && !longClickPassed && ((millis() - pushMillis) >= BUTTON_LONG_CLICK_MILLIS))
    {
        longClickPassed = true;
        return true;
    }

    return false;
}

bool Button::isClickRepeat()
{
    unsigned long currentMillis = millis();

    return pushed && ((currentMillis - pushMillis) >= BUTTON_REPEAT_MILLIS) &&
           (((currentMillis - pushMillis) % BUTTON_REPEAT_MILLIS) == 0);
}