# nexstarjoystick

Open source joystick device based on Arduino for Celestron Nexstar EVO

**Components:**
- [Arduino Pro Mini 5V](https://www.laskarduino.cz/arduino-pro-mini--atmega328-5v-16mhz--klon/)
- [joystick module](https://www.laskarduino.cz/petismerne-navigacni-tlacitko-joystick/)
- [Micro Step down](https://www.laskarduino.cz/mikro-step-down-menic--nastavitelny/) - set to 5v
- 7-segment display 17.4 mm x 12.5 mm, common cathode
- 8 pcs resistor 220
- Resistor 47k
- Diode 1N4148
- Cable with RJ12 connector

**RJ12 default coloring:**

**_But your cable may have a different order!_**
- 1 White
- 2 Black
- 3 Red
- 4 Green
- 5 Yellow
- 6 Blue

**Schematic:**

![](imgs/nexstarjoystick.png)

![](imgs/1.jpg)
