#ifndef __BUTTON_H__
#define __BUTTON_H__
#include <Arduino.h>

class Button
{
public:
    Button(uint8_t pin);
    void attach();
    virtual void refresh();
    bool isPushed();
    bool isChange();
    bool isShortClick();
    bool isLongClick();
    bool isClickRepeat();

public:
    uint8_t buttonPin = 0;
    bool pushed = false;
    bool prevPushed = false;
    unsigned long pushMillis = 0;
    bool longClickPassed = false;
};

#endif